#/usr/bin/env python3
# Author: Kenneth Liang(L), Justin Stewart, Franklin Smith
# Reference: https://gist.github.com/WPettersson/287de1e739d4d7869d555fd28ac587cf

# Make sure param.csv and payoff.csv are in the same folder. 
# User "python3 p1.py" to run the program

import cplex
import csv

# Parsing in the data from csv files
targets = 0
resources = 0
with open('param.csv', mode ='r') as csvfile:
  csv_reader = csv.reader(csvfile, delimiter=',')
  for row in csv_reader:
    targets = int(row[0])
    resources = int(row[1])

payoff = []
with open("payoff.csv", mode="r") as csvfile:
  csv_reader = csv.reader(csvfile, delimiter=",")
  for row in csv_reader:
    temp = []
    for item in row:
      temp.append(int(item))
    payoff.append(temp)  

# Promte the user to enter a target and calculate the LP of this target
attack_num = int(input("please enter a target number from 1 to %d: " %targets)) -1

# Create an instance of a linear problem to solve
problem = cplex.Cplex()

# We want to find a maximum of our objective function
problem.objective.set_sense(problem.objective.sense.maximize)

# The names of our variables
def_variables = []

for i in range(targets):
  def_variables.append("r" + str(i+1))
def_variables.append("C")

# The obective function. More precisely, the coefficients of the objective
# function. We build the objective function on the target the attack is going 
# attack, which is also the Expected Utility of the Defender
# The last variable is a dummy variable that represent a constant
# The expresion will be 0 coeffient for the rest of the variable that is not
# the current target being attack and the (Def_cover - Def_uncover) payoff
# value of the current target and Def_uncover payoff value for the constant
# ex. if attack_num = 2 
# the expression will be (Def_cover - Def_uncover)*(r2) + Def_uncover

objective = [0.0] * (targets + 1)
for i in range(targets):
  if(i == attack_num):
    objective[i] = payoff[attack_num][1]-payoff[attack_num][2]
  else:
    continue  
objective[targets] = payoff[attack_num][2]

# Lower bounds. All of them should be 0 except for the constant. 
# We want the constant to have a lower bound of 1

lower_bounds = []
for i in range(targets):
  lower_bounds.append(0.0)
lower_bounds.append(1.0)

# Upper bounds. All of them should be 0 except for the constant. 
# We want the constant to have a upper bound of 1
upper_bounds = []
for i in range(targets):
  upper_bounds.append(1.0)
upper_bounds.append(1.0)

problem.variables.add(obj = objective,
                      lb = lower_bounds,
                      ub = upper_bounds,
                      names = def_variables)

# Constraints

# First, we name the constraints
constraint_names = []
for i in range(targets):
  constraint_names.append("c" + str(i+1))


constraints = []
# This will be the constraints that the attacker expected utility of the 
# current target is greater than the expected utility of other targets
for i in range(targets):
  if i == attack_num:
    continue
  else:
    temp_coe = [0.0] * (targets + 1) 
    for j in range(targets + 1):
      if j == attack_num:
        temp_coe[j] = payoff[attack_num][4]-payoff[attack_num][3]
      elif j == i:
        temp_coe[j] = payoff[i][3]-payoff[i][4]
      elif j == targets:
        temp_coe[j] = payoff[attack_num][3]-payoff[i][3]
      else:
        temp_coe[j] = 0.0
    constraints.append([def_variables, temp_coe])


# This will be the limit constarint where the total value of all the variables
# cannot be greater than the value of resources
temp_limit_constarint = []
for i in range(targets):
  temp_limit_constarint.append(1.0)

temp_limit_constarint.append(0.0)

constraints.append([def_variables, temp_limit_constarint])

# This will be the right hand side of the constraints where they are all 0 
# other than the last constarint which is the number of resources
rhs = []

for i in range(targets-1):
  rhs.append(0)
rhs.append(resources)

# We need to enter the senses of the constraints. That is, we need to tell Cplex
# whether each constrains should be treated as an upper-limit (≤, denoted "L"
# for less-than), a lower limit (≥, denoted "G" for greater than) or an equality
# (=, denoted "E" for equality). In this case, all of the constarint needs to be 
# greater than 0 except for the last constarint which is less than the number of 
# resources
constraint_senses = []
for i in range(targets-1):
  constraint_senses.append("G")

constraint_senses.append("L")

# And add the constraints
problem.linear_constraints.add(lin_expr = constraints,
                               senses = constraint_senses,
                               rhs = rhs,
                               names = constraint_names)

# Solve the problem
problem.solve()

output = []
for i in range(targets):
  temp_list = []
  temp_list.append(i+1)
  temp_list.append(problem.solution.get_values("r" + str(i+1)))
  output.append(temp_list)

# And print the solutions
print(problem.solution.get_values())

with open("SSE.csv", mode='w') as writefile:
  wr = csv.writer(writefile)
  wr.writerows(output)